package no.hig.harkjerr.assigmentdemo;

import android.content.Intent;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Shows a map to the user of his/hers current location.
 * Much of this class was prewritten when selecting new > google
 * > google maps activity
 */
public class MapViewActivity extends FragmentActivity {

    public final static String MESSAGE = "no.hig.harkjerr.assigmentdemo.latlng";

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Location pos;

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);
        Intent intent = getIntent();
        pos = intent.getParcelableExtra(MainActivity.MESSAGE);

        setUpMapIfNeeded();

        if (mMap != null) {
            setUpMap(pos);
        }
    }
    /** Called when the activity is resumed. */
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        if (mMap != null) {
            setUpMap(pos);
        }
    }

    /**
     * Sets up the mapFragment if it is not already set up.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
        }
    }

    /**
     * Puts a marker of users location on the map and adjusts camera focus and zoom.
     * @param location the users location
     */
    private void setUpMap(final Location location) {

        final LatLng pos = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(pos)
                .title(getResources().getString(R.string.map_marker_my_location)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 13));
    }

    /**
     * Starts a new intent to show additional position data.
     * @param view reference to view that called the method
     */
    public void showAdditionalData(View view) {
        Intent intent = new Intent(this, PositionInformationActivity.class);
        intent.putExtra(MESSAGE, pos);
        startActivity(intent);
    }
}
