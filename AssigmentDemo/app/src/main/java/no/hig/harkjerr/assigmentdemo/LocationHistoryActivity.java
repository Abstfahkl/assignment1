package no.hig.harkjerr.assigmentdemo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Shows a list of the MyLocation objects stored in the
 * local database.
 */
public class LocationHistoryActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_history);

        DatabaseOperations dbHandler = new DatabaseOperations(this);
        List<MyLocation> locationList = dbHandler.findMyLocations();

        ListAdapter listAdapter = new LocationArrayAdapter(this, locationList);

        ListView listView = (ListView) findViewById(R.id.location_list_view);
        listView.setAdapter(listAdapter);
        dbHandler.close();

    }
    /**
     * Custom adapter to show data from list consisting of MyLocation objects.
     * Modified from http://stackoverflow.com/questions/2265661/how-to-use-arrayadaptermyclass
     */

    private class LocationArrayAdapter extends ArrayAdapter<MyLocation> {

        public LocationArrayAdapter(Context context, List<MyLocation> locations) {
            super(context, R.layout.row_layout, locations);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {

            LayoutInflater theInflater = LayoutInflater.from(getContext());
            View theView = theInflater.inflate(R.layout.row_layout, parent, false);
            String itemInfo = getItem(pos).getLocaleName() + ", "
                            + getItem(pos).getTemperatureCelsius() + " \u2103";

            TextView listItem = (TextView) theView.findViewById(R.id.list_item);
            listItem.setText(itemInfo);

            return theView;
        }

    }

}
