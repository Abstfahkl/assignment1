package no.hig.harkjerr.assigmentdemo;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class PositionInformationActivity extends Activity {

    Location pos;
    TextView positionData;
    int metersAboveOcean;

    String locationName;
    double temperature;


    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_position_information);

        Intent intent = getIntent();
        pos = intent.getParcelableExtra(MapViewActivity.MESSAGE);

        locationName = getLocaleFromLatLong(
                pos.getLatitude(), pos.getLongitude());
        metersAboveOcean = (int) pos.getAltitude();
        positionData = (TextView) findViewById(R.id.position_data);
        getLocation(pos);

    }

    /**
     * Reverse geocodes to locality based on position.
     * @param lat the latitude degree.
     * @param lng the longitude degree.
     * @return the locality name or null if geocoding fails.
     */
    private String getLocaleFromLatLong(double lat, double lng) {
        Geocoder geocoder = new Geocoder(this);
        String positionName = null;
        try {
            Address locationAddress = geocoder.getFromLocation(lat, lng, 1).get(0);
            positionName = locationAddress.getLocality();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return positionName;
    }

    /**
     * Makes a new MyLocation object with users location data and saves it
     * to the database. Then calls an intent to start locationHistoryActivity.
     * @param view reference to the view that called the method.
     */
    public void saveLocationData(View view) {
        MyLocation location = new MyLocation(locationName, pos.getLatitude(),
                pos.getLongitude(), pos.getAltitude(), temperature);    //add temp var
        DatabaseOperations dbHandler = new DatabaseOperations(this);
        dbHandler.addMyLocation(location);
        dbHandler.close();

        Intent intent = new Intent(this, LocationHistoryActivity.class);
        startActivity(intent);
    }

    /**
     * Attempts to update global temperature variable from an external xml resource.
     * @param location object containing the latitude and longitude values
     *                 required to set up the xml url.
     */
    public void getLocation(Location location) {

        String urlText = "http://api.openweathermap.org/data/2.5/weather?mode=xml&units=metric&lat="
        + location.getLatitude() + "&lon=" + location.getLongitude();

        try {
            new downloadTemperatureTask().execute(new URL(urlText));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Task responsible for downloading XML data from a given URL.
     * This task executes asynchronously and sets the UI textView
     * upon completion.
     */
    private class downloadTemperatureTask extends AsyncTask<URL, Void, String> {
        @Override
        protected String doInBackground(URL... urls) {
            assert urls.length == 1;
            return parseXML(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            temperature = Double.parseDouble(result);

            positionData.setText(
                    getResources().getString(R.string.info_text_1) + " "
                    + locationName + getResources().getString(R.string.info_text_2)
                    + " " + metersAboveOcean + " " + getResources().getString(R.string.info_text_3)
                    + " " + temperature + " \u2103"
            );
        }
    }

    /** Modified from imt3662_file_download example.
     * This function downloads and parses xml weather data from a specific
     * location passed in the url and returns a temperature value.
     * @param  url a String containing the URL to the weather xml resource
     * @return temp String containing temperature value.
     */
    private String parseXML(final URL url) {
        InputStream in;
        String temp = "";

        try {
            in = openHttpConnection(url);
            Document doc = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;

            try {
                db = dbf.newDocumentBuilder();
                doc = db.parse(in);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }

            doc.getDocumentElement().normalize();

            //---retrieve all the <item> nodes---
            NodeList itemNodes = doc.getElementsByTagName("temperature");
            Element node = (Element) itemNodes.item(0);
            temp = node.getAttribute("value");
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }

        return temp;
    }

    /**
     * From project imt3662_file_download example.
     * Handles some of the complexity of opening up a HTTP connection
     * @param  url   a String containing the absolute URL giving the base location and name of the content
     * @return in    an inputStream which will be the stream of text from the server
     */
    private InputStream openHttpConnection(final URL url) throws IOException {
        InputStream in = null;
        int response = -1;
        final URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }

        try {
            final HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return in;
    }


}
