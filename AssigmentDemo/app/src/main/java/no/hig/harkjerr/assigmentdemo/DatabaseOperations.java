package no.hig.harkjerr.assigmentdemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Heavily inspired by http://developer.android.com/training/basics/data-storage/databases.html
 * and http://www.techotopia.com/index.php/An_Android_Studio_SQLite_Database_Tutorial
 * Created by Sigurd on 08.09.2015.
 */
public class DatabaseOperations extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "locationHistory.db";

    private static final String CREATE_QUERY = "CREATE TABLE " +
            TableData.TableInfo.TABLE_NAME  + "(" +
            TableData.TableInfo.LOCALE_NAME + " TEXT," +
            TableData.TableInfo.LATITUDE    + " REAL," +
            TableData.TableInfo.LONGITUDE   + " REAL," +
            TableData.TableInfo.ALTITUDE    + " REAL," +
            TableData.TableInfo.TEMPERATURE + " REAL"  +
            " );";

    private static final String DELETE_QUERY = "DROP TABLE IF EXISTS " +
            TableData.TableInfo.TABLE_NAME + ");";

    /**
     * Constructor
     * @param context to use to open or create the database
     */
    public DatabaseOperations(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * called when the database is created for the first time.
     * Creates the database table.
     * @param db the database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
    }

    /**
     * Called when the database needs to be upgraded
     * @param db the database
     * @param oldVersion the old database version
     * @param newVersion the new database version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DELETE_QUERY);
        onCreate(db);
    }

    /**
     * Add a MyRecord object to the database
     * @param myLocation The object to be added to the database.
     */
    public void addMyLocation(MyLocation myLocation) {
        ContentValues values = new ContentValues();
        values.put(TableData.TableInfo.LOCALE_NAME, myLocation.getLocaleName());
        values.put(TableData.TableInfo.LATITUDE, myLocation.getLatitude());
        values.put(TableData.TableInfo.LONGITUDE, myLocation.getLongitude());
        values.put(TableData.TableInfo.ALTITUDE, myLocation.getAltitude());
        values.put(TableData.TableInfo.TEMPERATURE, myLocation.getTemperatureCelsius());

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TableData.TableInfo.TABLE_NAME, null, values);
        db.close();
    }

    /**
     * Finds all MyRecord objects in the database.
     * @return List of MyRecord objects
     */
    public List<MyLocation> findMyLocations() {
        String query = "SELECT * FROM " + TableData.TableInfo.TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        List<MyLocation> locationResults = new ArrayList<>();

        while(cursor.moveToNext()){
            locationResults.add(new MyLocation(
                    cursor.getString(0),
                    Double.parseDouble(cursor.getString(1)),
                    Double.parseDouble(cursor.getString(2)),
                    Double.parseDouble(cursor.getString(3)),
                    Double.parseDouble(cursor.getString(4))
            ));
        }
        cursor.close();
        return locationResults;
    }


}
