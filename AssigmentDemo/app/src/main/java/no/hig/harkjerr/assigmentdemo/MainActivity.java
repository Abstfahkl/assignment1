package no.hig.harkjerr.assigmentdemo;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * The main activity and entry point to the application.
 * http://blog.teamtreehouse.com/beginners-guide-location-android used
 * for finding GPS coordinates.
 */
public class MainActivity extends Activity implements
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public final static String MESSAGE = "no.hig.harkjerr.assigmentdemo.location";

    private GoogleApiClient apiClient;
    private Location location;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buildGoogleApiClient();
    }

    /** sets up and builds the api client. */
    protected synchronized void buildGoogleApiClient() {
        apiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /** Called when the activity is resumed. */
    @Override
    public void onResume() {
        super.onResume();
        apiClient.connect();
    }


    /** Creates an intent with the users position and starts the MapViewActivity activity
     *  if user position is available.
     * @param view reference to clicked widget.
     */
    public void showPositionOnMap(View view) {
        if(location != null) {
            Intent intent = new Intent(this, MapViewActivity.class);
            intent.putExtra(MESSAGE, location);
            startActivity(intent);
        }
        else
            Toast.makeText(this, R.string.notification_no_gps, Toast.LENGTH_SHORT).show();
    }

    /**
     *Called when the google api client method connect() is called.
     * @param connectionHint data from google play services.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        location = LocationServices.FusedLocationApi.getLastLocation(apiClient);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onConnectionSuspended(int i) { }

}
