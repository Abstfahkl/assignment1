package no.hig.harkjerr.assigmentdemo;

/**Stores data of users location and temperature.
 * Created by Sigurd on 09.09.2015.
 */
public class MyLocation {

    private String localeName;
    private double latitude;
    private double longitude;
    private double altitude;
    private double temperatureCelsius;

    public MyLocation(String localeName, double latitude,
                      double longitude, double altitude,
                      double temperatureCelsius) {
        this.localeName = localeName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.temperatureCelsius = temperatureCelsius;
    }

    public String getLocaleName() {
        return localeName;
    }

    public void setLocaleName(String localeName) {
        this.localeName = localeName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getTemperatureCelsius() {
        return temperatureCelsius;
    }

    public void setTemperatureCelsius(double temperatureCelsius) {
        this.temperatureCelsius = temperatureCelsius;
    }
}




