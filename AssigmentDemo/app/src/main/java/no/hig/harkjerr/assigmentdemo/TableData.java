package no.hig.harkjerr.assigmentdemo;

import android.provider.BaseColumns;

/** Contains final data for table and column names in database
 * Heavily inspired by http://developer.android.com/training/basics/data-storage/databases.html
 * and http://www.techotopia.com/index.php/An_Android_Studio_SQLite_Database_Tutorial
 * Created by Sigurd on 08.09.2015.
 */
public final class TableData {
    /** Empty Constructor */
    public TableData() {}

    public static abstract class TableInfo implements BaseColumns {
        public static final String LOCALE_NAME = "locale_name";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String ALTITUDE = "altitude";
        public static final String TEMPERATURE = "temperature";
        public static final String TABLE_NAME = "location_data";
    }

}
